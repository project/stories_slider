(function ($, Drupal) {
  Drupal.behaviors.swiperIntegration = {
    attach: function (context, settings) {
      initializeAllSwipers(context);
      handleAllSmallImageClicks(context);
      handleAllCloseButtonClicks(context);
      updatePaginationBackground(context);
    }
  };

  function initializeAllSwipers(context) {
    $('.story__slider', context).each(function () {
      initializeSwiper(this);
    });
  }

  function initializeSwiper(sliderElement) {
    if (!$(sliderElement).hasClass('swiper-initialized')) {
      $(sliderElement).addClass('swiper-initialized');
      new Swiper(sliderElement, {
        speed: 400,
        loop: true,
        autoplay: {
          delay: 2500,
          disableOnInteraction: false,
        },
        navigation: {
          nextEl: ".story__next",
          prevEl: ".story__prev",
        },
        pagination: {
          el: ".story__pagination",
          clickable: true,
          renderBullet: function (index, className) {
            return '<div class="' + className + '"> <div class="swiper-pagination-progress"></div> </div>';
          },
        },
        on: {
          slideChange: function () {
            updatePaginationBackground(this.el);
          }
        }
      });
    }
  }

  function handleAllSmallImageClicks(context) {
    $('.small-image', context).each(function () {
      handleSmallImageClick(this, context);
    });
  }

  function handleSmallImageClick(imageElement, context) {
    if (!$(imageElement).data('click-initialized')) {
      $(imageElement).data('click-initialized', true);
      $(imageElement).on('click', function () {
        const targetSelector = $(this).attr("data-story-target");
        const targetSwiperContainer = $(targetSelector, context);
        $(".story", context).css("display", "none");
        if (targetSwiperContainer.length) {
          targetSwiperContainer.css("display", "block");
          $("body").css("overflow", "hidden");
          updateSwiperOnVisible(targetSwiperContainer[0]);
          updatePaginationBackground(targetSwiperContainer[0]);
        }
      });
    }
  }

  function handleAllCloseButtonClicks(context) {
    $('.close-btn', context).each(function () {
      handleCloseButtonClick(this, context);
    });
  }

  function handleCloseButtonClick(buttonElement, context) {
    if (!$(buttonElement).data('close-initialized')) {
      $(buttonElement).data('close-initialized', true);
      $(buttonElement).on('click', function () {
        $(".story", context).css("display", "none");
        $("body").css("overflow", "");
        location.reload();
      });
    }
  }


  function updateSwiperOnVisible(swiperElement) {
    if (swiperElement.swiper) {
      swiperElement.swiper.update();
    }
  }

  function updatePaginationBackground(sliderElement) {
    const activeBullet = $(sliderElement).find('.swiper-pagination-bullet-active');
    const paginationBullets = $(sliderElement).find('.swiper-pagination-bullet');

    paginationBullets.removeClass('white-pagination');
    activeBullet.addClass('white-pagination');
  }

})(jQuery, Drupal);
