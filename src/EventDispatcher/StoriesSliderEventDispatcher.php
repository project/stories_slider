<?php

namespace Drupal\stories_slider\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\stories_slider\Event\StoriesSliderEvent;

/**
 * Handles the dispatching of StoriesSliderEvent events.
 */
class StoriesSliderEventDispatcher {
  /**
   * The event dispatcher service.
   *
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a StoriesSliderEventDispatcher object.
   *
   * @param EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   */
  public function __construct(EventDispatcherInterface $eventDispatcher) {
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Dispatches the StoriesSliderEvent and returns its data.
   *
   * @return array
   *   The data associated with the StoriesSliderEvent, as modified by event
   *   subscribers.
   */
  public function dispatchStoriesSliderEvent() {
    // Create a new instance of the StoriesSliderEvent.
    $event = new StoriesSliderEvent();

    // Dispatch the event and use the new constant for the event name.
    $this->eventDispatcher->dispatch($event, StoriesSliderEvent::NAME);

    // Retrieve the data modified by event subscribers.
    $data = $event->getData();

    // Return the modified data.
    return $data;
  }
}

