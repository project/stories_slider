<?php

namespace Drupal\stories_slider\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\stories_slider\EventDispatcher\StoriesSliderEventDispatcher; // Updated namespace
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Stories Slider' block.
 *
 * @Block(
 *   id = "stories_slider_block",
 *   admin_label = @Translation("Stories Slider Block"),
 *   category = @Translation("Custom")
 * )
 */
class StoriesSliderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The event dispatcher service.
   *
   * @var StoriesSliderEventDispatcher
   */
  protected $eventDispatcher;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, StoriesSliderEventDispatcher $eventDispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eventDispatcher = $eventDispatcher;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('stories_slider.event_dispatcher') // Updated service ID
    );
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * @return array
   *   A renderable array representing the content of the block.
   */
  public function build() {
    // Dispatches an event to gather stories data.
    $value = $this->eventDispatcher->dispatchStoriesSliderEvent(); // Updated method name

    // Constructs the render array for the block.
    $build = [
      '#theme' => 'stories_slider_block_template', // Updated theme hook suggestion
      '#content' => [
          '#main_images' => $value['main_images'],
          '#small_images' => $value['small_images'],
      ],
      // Attaches a library for additional JS/CSS.
      '#attached' => [
        'library' => [
          'stories_slider/stories', // Updated library
        ],
      ],
    ];

    return $build;
  }
}

