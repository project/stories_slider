<?php

namespace Drupal\stories_slider\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the Stories Slider event.
 */
class StoriesSliderEvent extends Event {
  /**
   * The event name.
   */
  const NAME = 'stories_slider.event';

  /**
   * Data associated with the event.
   *
   * @var array
   */
  protected $data;

  /**
   * Gets the data associated with the event.
   *
   * @return array
   *   The event data.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Sets the data associated with the event.
   *
   * @param array $data
   *   The data to set.
   */
  public function setData(array $data = []) {
    $this->data = $data;
  }
}

