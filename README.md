# Stories Module

The Stories module provides functionality to manage and display stories on your Drupal website.

## Table of contents

- Features
- Recommended modules
- Installation
- Usage
   1. Custom Block: Stories Block
   2. Event Handling
   3. Event Subscriber
- Dependency
- Maintainers

## Features

- Create and manage stories with main images and small thumbnail images.
- Display stories on various parts of your website using blocks.

## Recommended modules

No recommended modules.

## Installation

1. Download or clone this repository into the `modules` directory of your Drupal 10 installation.
2. Enable the Stories module through the Drupal administration interface (`/admin/modules`).

## Usage

Once installed and enabled, you can utilize the Stories module to create and display stories on your Drupal website. The module provides a custom block named "Stories Block" that you can place in various regions of your site.

### Custom Block: Stories Block

The "Stories Block" displays a collection of stories in an image slider format. It fetches data associated with the stories using event dispatching.

### Event Handling

The module defines a custom event named `StoriesEvent`, which is dispatched to gather data for displaying stories. The `StoriesEventDispatcher` class handles the dispatching of this event.

### Event Subscriber

The `StoriesEventSubscriber` class subscribes to the `StoriesEvent` to provide image data for displaying stories. It listens for the `stories.event` event and responds by setting the necessary data for the event.

#### Data Format

The event subscriber provides data in the following format:

<code>
  'main_images' => [
    // Array of main images for each story.
    [
      // Array of individual images within the story.
      [
        'url' => 'path/to/image1.jpg',
        'alt' => 'Alt text 1',
      ],
    ],
    // Add more stories with main images as needed.
  ],
  'small_images' => [
    // Array of small thumbnail images corresponding to each main image.
    [
      'url' => 'path/to/thumbnail1.jpg',
      'alt' => 'Thumbnail 1',
    ],
    // Add more thumbnail images as needed.
  ],
</code>

## Dependency

The module integrates with the Swiper library to create image sliders for the stories. Swiper provides features like autoplay, navigation, and pagination.

## Current maintainers:

  1. Shubham Pareek - https://www.drupal.org/u/shubham_pareek_19
  2. Vivek Panicker - https://www.drupal.org/u/vivek-panicker
